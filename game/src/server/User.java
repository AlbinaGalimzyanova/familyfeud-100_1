package server;

import logics.Questions;
import server.room.Room;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class User extends Thread {
    private BufferedReader reader;
    private PrintWriter writer;
    private final Room room;
    private String userName;
    private int score;
    private boolean iGameFinishedForSingleUser;
    private Map<String, Map<String, Integer>> questions = new HashMap<>();

    User(Socket newConnection, Room room) throws IOException {
        reader = new BufferedReader(new InputStreamReader(newConnection.getInputStream()));
        writer = new PrintWriter(newConnection.getOutputStream(), true);
        score = 0;
        this.room = room;

        Questions questions = new Questions();
        this.questions = questions.generateQuestions();
    }

    public int getScore() {
        return score;
    }

    public boolean getIsGameFinishedForSingleUser() {
        return iGameFinishedForSingleUser;
    }

    @Override
    public void run() {
        try {
            iGameFinishedForSingleUser = false;
            writer.println("Добро пожаловать в игру 100 к 1!");
            writer.println("Пожалуйста, представьтесь.");
            userName = reader.readLine();
            System.out.println("Игрок " + userName + " присоединился к серверу.");
            writer.println("Ваша задача - угадать самые популярные ответы на вопросы. За каждый угаданный ответ вы " +
                    "получаете от 40 до 240 баллов \n(чем менее популярный ответ вы отгадаете, тем больше баллов " +
                    "наберёте). Удачи!");

            synchronized (Server.class) {
                try {
                    room.addUser(this);

                    if (room.isFull()) {
                        Server.getNotFullRoom();
                        room.start();
                    }

                    Server.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            while (!room.isGameFinished() && !questions.isEmpty()) {
                HashMap.Entry<String, Map<String, Integer>> entry = questions.entrySet().iterator().next();
                sendMessage(entry.getKey());
                Map<String, Integer> answers = entry.getValue();
                questions.remove(entry.getKey());
                String userMessage = reader.readLine();

                int tries = 20;
                boolean isGuessed = false;

                while (tries > 0 && !isGuessed) {
                    if (answers.containsKey(userMessage)) {
                        sendMessage("Верно! Вы набрали " + answers.get(userMessage) + " баллов.");

                        score += answers.get(userMessage);
                        answers.remove(userMessage);

                        if (answers.isEmpty()) {
                            isGuessed = true;
                            sendMessage("Отлично, вы угадали все ответы! Переходим к следующему вопросу.");
                            break;
                        }

                        sendMessage("Попробуйте отгадать и другие ответы!");

                        userMessage = reader.readLine();
                    } else {
                        sendMessage("Такого ответа нет, попробуйте ещё раз (количество оставшихся попыток: " + tries + ")");
                        userMessage = reader.readLine();
                    }

                    tries--;
                }

                if (tries == 0 && !isGuessed) {
                    sendMessage("Попытки исчерпаны, переходим к следующему вопросу!");
                }
            }
        } catch (IOException e) {
            System.out.println("Проблемы подключения с игроком " + userName);
        } finally {
            sendMessage("Упс, вопросы закончились, посчитаем, сколько очков вы набрали!");
            sendMessage((String.valueOf(score)));

            iGameFinishedForSingleUser = true;

            synchronized (room) {
                room.notify();
            }
        }
    }

    @Override
    public String toString() {
        return String.format("%s %d", userName, score);
    }

    public void sendMessage(String message) {
        writer.println(message);
    }
}