package server.room;

import logics.Game;
import server.Server;
import server.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.IntStream;

public class Room extends Thread {
    private ArrayList<User> users;
    private int usersCount;
    private RoomStatus roomStatus;
    private Game game;

    public Room() {
        users = new ArrayList<>();
        IntStream.iterate(0, i -> i + 1)
                .limit(2)
                .forEach(i -> users.add(null));
        usersCount = 0;
        roomStatus = RoomStatus.WAITING_FOR_CONNECTIONS;
    }

    @Override
    public void run() {
        boolean flag = true;
        System.out.println("Игра началась!");

        users.forEach(user -> user.sendMessage("Игра началась!"));

        roomStatus = RoomStatus.GAMING;

        synchronized (Server.class) {
            game = new Game();
            Server.class.notifyAll();
        }

        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (User user : users) {
                if (user.getIsGameFinishedForSingleUser()) {
                    flag = false;
                }
            }

            if (flag) {
                roomStatus = RoomStatus.FINISHED;
            }
        }

        /*if (roomStatus == RoomStatus.FINISHED) {
            users.forEach(user -> user.sendMessage("Игра закончена!"));
            users.forEach(user -> user.sendMessage(this.getRating()));
        }*/

        System.out.println("Один из игроков закончил игру, текущий рейтинг:");
        System.out.println(getRating());
        System.out.println("Чтобы узнать текущий рейтинг, введите команду \"Рейтинг\"");
    }

    public synchronized void addUser(User user) {
        if (usersCount >= 2) Server.getNotFullRoom().addUser(user);

        int emptyPlaceIndex = IntStream.iterate(0, index -> index + 1)
                .limit(users.size())
                .filter(index -> users.get(index) == null)
                .findFirst()
                .orElse(-1);

        if (emptyPlaceIndex == -1) return;

        users.set(emptyPlaceIndex, user);
        usersCount++;
    }

    public String getRating() {
        return users.stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.comparingInt(User::getScore))
                .map(User::toString)
                .reduce("", (acc, userRow) -> acc + userRow + "\n");
    }

    public boolean isFull() {
        return usersCount == 2;
    }

    public boolean isGameFinished() {
        return roomStatus == RoomStatus.FINISHED;
    }
}