package server.room;

public enum RoomStatus {
    WAITING_FOR_CONNECTIONS,
    GAMING,
    FINISHED
}