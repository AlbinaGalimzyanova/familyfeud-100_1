package logics;

import java.util.HashMap;
import java.util.Map;

public class Questions {
    private Map<String, Map<String, Integer>> questions = new HashMap<>();

    public Map<String, Map<String, Integer>> generateQuestions() {
        Map<String, Integer> answers1 = new HashMap<>();

        answers1.put("Мышь", 40);
        answers1.put("Сон", 80);
        answers1.put("Рыба", 120);
        answers1.put("Кот", 160);
        answers1.put("Мясо", 200);
        answers1.put("Корм", 240);
        questions.put("Что снится кошке?", answers1);

        Map<String, Integer> answers2 = new HashMap<>();
        answers2.put("Кино", 40);
        answers2.put("Зебра", 80);
        answers2.put("Телевизор", 120);
        answers2.put("Фотография", 160);
        answers2.put("Жизнь", 200);
        answers2.put("Кот", 240);
        questions.put("Что или кто бывает чёрно-белым?", answers2);

        Map<String, Integer> answers3 = new HashMap<>();
        answers3.put("Людей", 40);
        answers3.put("Баранов", 80);
        answers3.put("Облака", 120);
        answers3.put("Звёзды", 160);
        answers3.put("Волков", 200);
        answers3.put("Себя", 240);
        questions.put("Что считают овцы, чтобы заснуть?", answers3);

        /*Map<String, Integer> answers4 = new HashMap<>();

        answers4.put("Курить трубку", 40);
        answers4.put("Играть на скрипке", 80);
        answers4.put("Искать преступника", 120);
        answers4.put("Думать", 160);
        answers4.put("Читать", 200);
        answers4.put("Есть овсянку", 240);
        questions.put("Что любил делать Шерлок Холмс?", answers4);

        Map<String, Integer> answers5 = new HashMap<>();
        answers5.put("Дождя", 40);
        answers5.put("Музыки", 80);
        answers5.put("Тишины", 120);
        answers5.put("Природы", 160);
        answers5.put("Телевизора", 200);
        answers5.put("Моря", 240);
        questions.put("Под звуки чего хорошо спать?", answers5);

        Map<String, Integer> answers6 = new HashMap<>();
        answers6.put("Денег", 40);
        answers6.put("Здоровья", 80);
        answers6.put("Счастья", 120);
        answers6.put("Квартиру", 160);
        answers6.put("Машину", 200);
        answers6.put("Любви", 240);
        questions.put("Что бы вы попросили у золотой рыбки?", answers6);

        Map<String, Integer> answers7 = new HashMap<>();
        answers7.put("Крупская", 40);
        answers7.put("Сталин", 80);
        answers7.put("Дети", 120);
        answers7.put("Троцкий", 160);
        answers7.put("Народ", 200);
        answers7.put("Дзержинский", 240);
        questions.put("Кто дружил с Лениным?", answers7);

        Map<String, Integer> answers8 = new HashMap<>();
        answers8.put("Кататься на коньках", 40);
        answers8.put("Кататься на лыжах", 80);
        answers8.put("Играть в снежки", 120);
        answers8.put("Кататься на санках", 160);
        answers8.put("Кататься с горки", 200);
        answers8.put("Гулять", 240);
        questions.put("Лучшие развлечения в новогодние каникулы", answers8);

        Map<String, Integer> answers9 = new HashMap<>();
        answers9.put("Паук", 40);
        answers9.put("Таракан", 80);
        answers9.put("Комар", 120);
        answers9.put("Богомол", 160);
        answers9.put("Жук", 200);
        answers9.put("Муха", 240);
        questions.put("У какого насекомого самый жуткий вид?", answers9);

        Map<String, Integer> answers10 = new HashMap<>();
        answers10.put("Водой", 40);
        answers10.put("Пеной", 80);
        answers10.put("Огнетушителем", 120);
        answers10.put("Песком", 160);
        answers10.put("Землей", 200);
        answers10.put("Одеялом", 240);
        questions.put("Чем можно потушить пожар?", answers10);*/

        return questions;
    }
}