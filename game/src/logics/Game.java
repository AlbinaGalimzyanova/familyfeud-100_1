package logics;

import java.util.HashMap;
import java.util.Map;

public class Game {
    private Map<String, Map<String, Integer>> questions = new HashMap<>();

    public Game() {
        Questions questions = new Questions();
        this.questions = questions.generateQuestions();
    }
}