import server.Server;

import java.io.IOException;

public class ApplicationRunner {
    public static void main(String[] args) throws IOException {
        Server server = new Server(1000);
        server.up();
    }
}